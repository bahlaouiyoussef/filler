/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/27 14:30:19 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/27 14:30:21 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GAME_H
# define GAME_H

# include "helper.h"
# include "piece.h"
# include "libft.h"
# include "board.h"

typedef struct	s_game
{
	t_board		*board;
	t_piece		*piece;
	char		me;
	char		enemy;
	int			score;
}				t_game;

t_game			*ft_gamenew(void);
int				ft_gameinit(t_game *game, const int fd);
void			ft_gamedel(t_game **game);

#endif
