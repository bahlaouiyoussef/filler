/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   board.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/27 14:30:10 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/27 14:30:12 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BOARD_H
# define BOARD_H

# include "libft.h"

typedef struct	s_board
{
	char	**board;
	int		rows;
	int		cols;
}				t_board;

t_board			*ft_boardnew(int rows, int cols);
int				ft_boardinit(t_board *board, int enemy, const int fd);
void			ft_boarddel(t_board **board);

#endif
