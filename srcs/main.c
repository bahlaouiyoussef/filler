/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.1337.ma>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/27 16:02:54 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/10/04 14:23:18 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include "libft.h"
#include "game.h"

static int	ft_canplace(t_game *game, int row, int col)
{
	int		i;
	int		y;
	int		x;
	int		inter;
	int		score;

	i = 0;
	inter = 0;
	score = 0;
	while (i < game->piece->size)
	{
		y = row + game->piece->coords[i].y;
		x = col + game->piece->coords[i].x;
		if (x < 0 || x >= game->board->cols)
			return (-1);
		if (y < 0 || y >= game->board->rows)
			return (-1);
		if (game->board->board[y][x] == game->enemy)
			return (-1);
		if ((int)game->board->board[y][x] >= 0)
			score += (int)game->board->board[y][x];
		inter += (int)game->board->board[y][x] == (int)game->me;
		i++;
	}
	return (inter == 1 ? score : -1);
}

static void	ft_getrowcol(t_game *game, int *row, int *col)
{
	int	r;
	int	c;
	int	score;

	r = 0;
	game->score = 10e7;
	while (r < game->board->rows)
	{
		c = 0;
		while (c < game->board->cols)
		{
			if ((score = ft_canplace(game, r, c)) >= 0)
			{
				if (score < game->score)
				{
					game->score = score;
					*row = r;
					*col = c;
				}
			}
			c++;
		}
		r++;
	}
}

static void	ft_play(t_game *game)
{
	int	row;
	int	col;

	while (1)
	{
		if (ft_gameinit(game, 0) == 0)
		{
			row = 0;
			col = 0;
			ft_getrowcol(game, &row, &col);
			row -= game->piece->min_row;
			col -= game->piece->min_col;
			ft_printf("%d %d\n", row, col);
		}
		else
			break ;
	}
}

int			main(void)
{
	t_game	*game;

	game = ft_gamenew();
	if (game == NULL)
		return (1);
	ft_play(game);
	ft_gamedel(&game);
	return (0);
}
