/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_boarddel.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/27 15:26:09 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/27 15:26:10 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "board.h"

void	ft_boarddel(t_board **board)
{
	int		i;

	if (board == NULL || *board == NULL)
		return ;
	i = 0;
	while (i < (*board)->rows)
	{
		ft_strdel((*board)->board + i);
		i++;
	}
	free((*board)->board);
	ft_memdel((void**)board);
}
