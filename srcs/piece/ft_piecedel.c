/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_piecedel.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/27 15:34:24 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/27 15:53:22 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "piece.h"

void	ft_piecedel(t_piece **piece)
{
	if (piece == NULL || *piece == NULL)
		return ;
	ft_memdel((void**)&(*piece)->coords);
	ft_memdel((void**)piece);
}
