/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vectordel.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/27 15:57:49 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/27 15:57:49 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "vector.h"

void	ft_vectordel(t_vector **vector)
{
	if (vector == NULL)
		return ;
	ft_memdel((void**)vector);
}
