/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_gameinit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.1337.ma>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/27 16:01:02 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/10/04 14:20:58 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

static int	ft_getplayer(const int fd, char *me, char *enemy)
{
	char	*line;
	int		error;
	int		player_num;

	if (!(error = ft_getline(fd, &line) <= 0))
	{
		error = ft_strstartswith(line, "$$$ exec p") == 0;
		if (error == 0)
		{
			player_num = ft_atoi(line + 10);
			error = player_num != 1 && player_num != 2;
			if (error == 0)
			{
				*me = -2;
				*enemy = -1;
				if (player_num == 1)
				{
					*me = -1;
					*enemy = -2;
				}
			}
		}
		ft_strdel(&line);
	}
	return (error);
}

static int	ft_getboard(t_game *game, const int fd)
{
	char	*line;
	int		error;
	int		rows;
	int		cols;

	if ((error = ft_getline(fd, &line) != 1) == 0)
	{
		error = ft_strstartswith(line, "Plateau ") == 0;
		if (error == 0)
			error = ft_isnumber(line + 8, ' ') == 0;
		if (error == 0)
			rows = ft_atoi(line + 8);
		if (error == 0)
			error = ft_isnumber(line + 8 + ft_nbrlen(rows) + 1, ':') == 0;
		if (error == 0)
			cols = ft_atoi(line + 8 + ft_nbrlen(rows));
		if (error == 0 && game->board == NULL)
			game->board = ft_boardnew(rows, cols);
		error = game->board == NULL;
		if (error == 0)
			error = ft_boardinit(game->board, game->enemy, fd) != 0;
		ft_strdel(&line);
	}
	return (error);
}

static int	ft_getpiece(t_game *game, const int fd)
{
	int		error;

	ft_piecedel(&game->piece);
	game->piece = ft_piecenew();
	error = game->piece == NULL;
	if (error == 0)
		error = ft_pieceinit(game->piece, fd);
	return (error);
}

int			ft_gameinit(t_game *game, const int fd)
{
	int		error;

	error = 0;
	if (game->me == '\0' || game->enemy == '\0')
		error = ft_getplayer(fd, &game->me, &game->enemy);
	if (error == 0)
		error = ft_getboard(game, fd);
	if (error == 0)
		error = ft_getpiece(game, fd);
	return (error);
}
