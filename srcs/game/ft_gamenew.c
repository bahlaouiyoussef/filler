/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_gamenew.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/27 16:01:30 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/27 16:01:31 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game.h"

t_game	*ft_gamenew(void)
{
	t_game	*game;

	game = (t_game*)ft_memalloc(sizeof(t_game));
	return (game);
}
