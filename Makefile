NAME = ybahlaou.filler

FLAGS = -Wall -Wextra -Werror

SRCS =	srcs/main.c				\
		srcs/game/game.a		\
		srcs/board/board.a		\
		srcs/piece/piece.a		\
		srcs/vector/vector.a	\
		srcs/helper/helper.a	\
		libft/libft.a

INCLUDES = -I includes/

all: compile_all $(NAME)

$(NAME): $(SRCS)
	@gcc -o $(NAME) $(FLAGS) $(SRCS) $(INCLUDES)

compile_all:
	make -C libft/
	make -C srcs/game/
	make -C srcs/board/
	make -C srcs/piece/
	make -C srcs/vector/
	make -C srcs/helper/

clean:
	make -C libft/ clean
	make -C srcs/game/ clean
	make -C srcs/board/ clean
	make -C srcs/piece/ clean
	make -C srcs/vector/ clean
	make -C srcs/helper/ clean

fclean:
	make -C libft/ fclean
	make -C srcs/game/ fclean
	make -C srcs/board/ fclean
	make -C srcs/piece/ fclean
	make -C srcs/vector/ fclean
	make -C srcs/helper/ fclean
	rm -rf $(NAME)

re: fclean all
