/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:16:53 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:16:54 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char *ptr_s;

	ptr_s = (unsigned char*)s;
	while (n >= 1)
	{
		if (*ptr_s == (unsigned char)c)
			return ((void*)ptr_s);
		n--;
		ptr_s++;
	}
	return (NULL);
}
