/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_btreepreorder.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:15:49 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:15:50 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_btreepreorder(t_btree *root, void (*ft)(t_btree*))
{
	if (root == NULL)
		return ;
	(*ft)(root);
	ft_btreepreorder(root->left, ft);
	ft_btreepreorder(root->right, ft);
}
