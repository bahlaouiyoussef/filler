/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:17:43 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:17:44 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*newstr;
	size_t	newstr_len;
	size_t	i;

	newstr_len = ft_strlen(s);
	newstr = ft_strnew(newstr_len);
	if (newstr != NULL)
	{
		i = 0;
		while (*(s + i))
		{
			*(newstr + i) = (*f)(*(s + i));
			i++;
		}
		*(newstr + i) = '\0';
	}
	return (newstr);
}
