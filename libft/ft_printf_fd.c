/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:17:02 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:17:02 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_printf_fd(int fd, const char *format, ...)
{
	va_list	args;

	va_start(args, format);
	ft_vprintf_fd(fd, format, args);
	va_end(args);
}
