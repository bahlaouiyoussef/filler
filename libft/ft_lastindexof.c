/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lastindexof.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:16:15 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:16:16 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

ssize_t	ft_lastindexof(const char *str, char c)
{
	size_t	i;
	size_t	index;

	i = 0;
	index = -1;
	while (*str)
	{
		if (*str == c)
			index = i;
		str++;
		i++;
	}
	return (index);
}
