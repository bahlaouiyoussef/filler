/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:17:58 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:17:59 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *str, const char *to_find, size_t len)
{
	size_t	i;
	size_t	j;

	if (!*to_find)
		return ((char*)str);
	i = 0;
	while (*str && i < len)
	{
		j = 0;
		while (*(str + j) && *(to_find + j) &&
				i + j < len && *(to_find + j) == *(str + j))
			j++;
		if (!*(to_find + j))
			return ((char*)str);
		i++;
		str++;
	}
	return (NULL);
}
