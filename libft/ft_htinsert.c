/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_htinsert.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ybahlaou <ybahlaou@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/28 10:16:00 by ybahlaou          #+#    #+#             */
/*   Updated: 2019/09/28 10:16:01 by ybahlaou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_hashkey(const char *key)
{
	int	sum;

	sum = 0;
	while (*key)
	{
		sum += *key;
		key++;
	}
	return (sum);
}

void		ft_htinsert(t_hashtable *ht, const char *key, const char *value)
{
	int			index;
	t_hentry	*cur;

	index = ft_hashkey(key) % ht->size;
	cur = ht->table[index];
	if (cur == NULL)
		ht->table[index] = ft_hentrynew(key, value);
	else
	{
		cur = ft_htget(ht, key);
		if (cur != NULL)
		{
			ft_strdel(&cur->value);
			cur->value = ft_strdup(value);
		}
		else
		{
			cur = ft_hentrynew(key, value);
			cur->next = ht->table[index];
			ht->table[index] = cur;
		}
	}
}
